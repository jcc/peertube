#!/usr/bin/env python3
# coding: utf8

import argparse
from pathlib import Path
from urllib.parse import urljoin

import yaml


def index(list_, key='url'):
    by_url = {}
    for video in list_:
        url = video[key]
        by_url[url] = video
    return by_url


def merge(meta, pt_meta):
    """Merge videos from conference into our existing PeerTube metadata."""
    conference = meta['conference']
    for video in meta['videos']:
        video['url'] = urljoin(conference['video_base'], video['video'])
    meta_index = index(meta['videos'])
    orphans_index = index(pt_meta.get('orphans', []))
    pt_index = index(pt_meta.setdefault('videos', []))

    # Find orphans and archive them
    for url in set(pt_index) - set(meta_index):
        video = pt_index.pop(url)
        pt_meta['videos'].remove(video)
        orphans_index[url] = video
        pt_meta.setdefault('orphans', []).append(video)

    # Add missing videos
    for url in set(meta_index) - set(pt_index):
        video = {'url': url}
        pt_meta['videos'].append(video)
        pt_index[url] = video

    # Order the PT meta list like meta
    pt_meta['videos'] = [pt_index[video['url']] for video in meta['videos']]

    # Merge metadata from meta into pt_meta, for each video
    for video in pt_meta['videos']:
        meta_video = meta_index[video['url']]
        update_video(meta_video, video, conference)


def update_video(meta_video, video, conference):
    """Update video with details from meta_video"""
    name = meta_video['title']
    video['name'] = truncate(name, 120)

    description = []
    if video['name'] != name:
        description.append(name)
    if meta_video.get('speakers'):
        for speaker in meta_video['speakers']:
            description.append('by {}'.format(speaker))
        description.append('')

    if conference.get('title'):
        description.append(
            'At: {}'.format(conference['title']))
    if meta_video.get('details_url'):
        description.append(meta_video['details_url'])
        if (conference.get('website') and not
                meta_video['details_url'].startswith(conference['website'])):
            description.append(conference['website'])
    elif conference.get('website'):
        description.append(conference['website'])

    if meta_video.get('description'):
        description.append('')
        description.append(meta_video['description'])
        description.append('')

    if meta_video.get('room'):
        description.append('Room: {}'.format(meta_video['room']))
    if meta_video.get('start'):
        description.append(
            'Scheduled start: {}'.format(meta_video['start']))
    description = '\n'.join(description)
    video['description'] = truncate(description, 10000)


def truncate(string, length):
    """PeerTube has length limits for things like names"""
    if len(string) <= length:
        return string
    string = string[:length-1] + '…'
    return string


def main():
    parser = argparse.ArgumentParser(
        description='Merge metadata from the archive-meta repo into here')
    parser.add_argument('archive-meta', metavar='LOCATION',
                        help='Location of the archive-meta metadata')
    args = parser.parse_args()

    base = Path(getattr(args, 'archive-meta'))
    for conf_meta_path in base.glob('*/*.yml'):
        relative_path = conf_meta_path.relative_to(base)
        print('Merging ', relative_path)
        with conf_meta_path.open() as f:
            conf_meta = yaml.safe_load(f)

        video_path = Path('metadata') / relative_path

        video_meta = {}
        if video_path.exists():
            with video_path.open() as f:
                video_meta = yaml.safe_load(f)

        merge(conf_meta, video_meta)

        if not video_meta['videos']:
            if video_path.exists():
                video_path.unlink()
            continue

        video_path.parent.mkdir(exist_ok=True)

        with video_path.open('w') as f:
            f.write('---\n')
            yaml.safe_dump(
                video_meta, f, allow_unicode=True, default_flow_style=False)


if __name__ == '__main__':
    main()

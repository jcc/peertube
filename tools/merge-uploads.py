#!/usr/bin/env python3

import argparse

import yaml


def main():
    parser = argparse.ArgumentParser(
        description='Merge an upload log into a metadata file')
    parser.add_argument('meta', metavar='META', type=argparse.FileType('r+'),
                        help='Metadata file that was uploaded')
    parser.add_argument('log', metavar='LOG', type=open,
                        help='Upload log')
    args = parser.parse_args()

    meta = yaml.safe_load(args.meta)
    log = yaml.safe_load_all(args.log)

    assert 'defaults' in meta

    ids = {}
    for entry in log:
        url = entry['url']
        ids[url] = entry['peertube_id']

    args.log.close()

    for video in meta['videos']:
        url = video['url']
        if url in ids:
            video['peertube_id'] = ids[url]

    args.meta.seek(0)
    args.meta.truncate()
    args.meta.write('---\n')
    yaml.safe_dump(
        meta, args.meta, allow_unicode=True, default_flow_style=False)


if __name__ == '__main__':
    main()

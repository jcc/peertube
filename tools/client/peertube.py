from pathlib import Path
from urllib.parse import urljoin, urlparse
import json
import logging
import time

from oauthlib.oauth2 import LegacyApplicationClient
from requests_oauthlib import OAuth2Session
import requests

from client.errors import PeerTubeError, RateLimitedException


BASEDIR = Path(__file__).parent / '..' / '..'
CLIENT_CONFIG_FILE = BASEDIR / 'client_id.json'
TOKEN_FILE = BASEDIR / 'token.json'

log = logging.getLogger(__name__)
_session = None


class PeerTubeSession(OAuth2Session):
    def __init__(self, load_token=True):
        client = self._load_client()
        super().__init__(
            client=client,
            auto_refresh_url=self._token_url,
            auto_refresh_kwargs={
                'client_id': client.client_id,
                'client_secret': self._client_secret,
            },
            token_updater=self._save_token)
        if load_token:
            self._load_token()

    def _load_client(self):
        with CLIENT_CONFIG_FILE.open() as f:
            config = json.load(f)
        self.base_url = config['base_url']
        self.api_base_url = config['api_base_url']
        self._client_secret = config['client_secret']
        return LegacyApplicationClient(client_id=config['client_id'])

    def _load_token(self):
        """Load a token from disk"""
        with TOKEN_FILE.open() as f:
            self.token = json.load(f)

    def _save_token(self, token):
        """Save a new token to disk"""
        # PeerTube just provides relative time, on refresh
        token.setdefault('expires_at', time.time() + token['expires_in'] - 10)

        # Less useful on disk...
        token.pop('expires_in')

        with TOKEN_FILE.open('w') as f:
            json.dump(token, f, indent=2)
            f.write('\n')
        log.debug('Updated %s', TOKEN_FILE)

        self.token = token

    @property
    def _token_url(self):
        return urljoin(self.api_base_url, 'users/token')

    def fetch_token(self, username, password):
        """Get a new token from the server.
        Cache on disk.
        """
        token = super().fetch_token(
            self._token_url,
            username=username,
            password=password,
            client_secret=self._client_secret,
        )
        self._save_token(token)
        return token

    def request(self, method, url, *args, **kwargs):
        if not urlparse(url).scheme:
            url = urljoin(self.api_base_url, url)

        r = super().request(method, url, *args, **kwargs)

        if r.status_code == 429:
            raise RateLimitedException()

        if r.headers.get('Content-Type') == 'application/json':
            body = r.json()
            if body.get('error'):
                raise PeerTubeError(body['error'], body['code'])

        if r.status_code >= 300:
            raise PeerTubeError(
                r.content or 'HTTP %i' % r.status_code, r.status_code)

        return r


def fetch_client_keypair(base_url):
    """Get a client keypair from the server, and save to disk.
    It seems to give the same one to everyone, but meh.
    """
    api_base_url = urljoin(base_url, 'api/v1/')
    url = urljoin(api_base_url, 'oauth-clients/local')
    log.debug('Getting a keypair from %s', url)
    r = requests.get(url)
    assert r.status_code == 200
    client_config = r.json()
    client_config['base_url'] = base_url
    client_config['api_base_url'] = api_base_url

    with CLIENT_CONFIG_FILE.open('w') as f:
        json.dump(client_config, f, indent=2)
        f.write('\n')
    log.debug('Written %s', CLIENT_CONFIG_FILE)


def get_session():
    """Return a PeerTubeSession() singleton"""
    global _session
    if not _session:
        _session = PeerTubeSession()
    return _session

import logging

from client.constants import VideoPrivacy, VideoState
from client.peertube import get_session

log = logging.getLogger(__name__)


class Video:
    def __init__(self, data):
        self._pt = get_session()
        self.data = data

    @property
    def uuid(self):
        return self.data['uuid']

    @property
    def name(self):
        return self.data['name']

    @property
    def url(self):
        return '{}/videos/watch/{}'.format(self._pt.base_url, self.uuid)

    @property
    def privacy(self):
        return VideoPrivacy(self.data['privacy']['id'])

    @property
    def state(self):
        return VideoState(self.data['state']['id'])

    def set_description(self, description):
        self.update(description=description)

    def update(self, **data):
        self._pt.put('videos/{}'.format(self.uuid), data=data)
        self.data.update(data)

    def publish(self, privacy=VideoPrivacy.PUBLIC):
        self.update(privacy=int(privacy))
        log.info('Changed privacy on %s to %s', self.name, privacy)

    def delete(self):
        self._pt.delete('videos/{}'.format(self.uuid))

    @classmethod
    def get(cls, id_):
        peertube = get_session()
        response = peertube.get('videos/{}'.format(id_)).json()
        return Video(response)

    @classmethod
    def get_categories(cls):
        peertube = get_session()
        return peertube.get('videos/categories').json()

    @classmethod
    def get_licences(cls):
        peertube = get_session()
        return peertube.get('videos/licences').json()

    @classmethod
    def import_video(cls, channel_id, url, name, **kwargs):
        log.info('Importing %s to %s from %s', name, channel_id, url)
        peertube = get_session()
        data = {
            'channelId': channel_id,
            'name': name,
            'targetUrl': url,
        }
        data.update(kwargs)
        response = peertube.post('videos/imports', data=data).json()
        return Video(response['video'])

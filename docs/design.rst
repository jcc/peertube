Design
======

Data is stored in YAML, in git, for ease of contribution and
collaboration.

This is just a simple set of scripts for manipulating this YAML
metadata.

The tools:

``merge-meta``
--------------

Create/Update a PeerTube metadata file for each set of videos in the
``archive-meta`` repo.


``auth``
--------

Authenticate to PeerTube, write, ``client_id.json`` and ``token.json``.
Use this tool to create / verify upload credentials.

``upload_peertube``
------------------

Upload all videos in a metadata file to PeerTube, setting metadata
appropriately.

Videos that have a ``peertube_id`` attribute are already uploaded, and
skipped.

Produce a log file of completed upload video IDs.

``merge-uploads``
-----------------

Merge the video IDs from ``upload_peertube`` into a PeerTube metadata
file, setting ``peertube_id`` attributes.

``publish-slowly``
------------------

Slowly work through the videos in a PeerTube metadata file, publishing 1
previously unlisted video, a day.
This is friendlier to the PeerTube channel subscribers, than huge batches
of video.

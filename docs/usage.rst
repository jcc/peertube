Usage example
=============

As PeerTube can import videos by URL, uploads can be done easily from
anywhere, pulling videos from the DebConf public video archive.

Once-off setup
--------------

.. _credential-creation:

PeerTube authentication: credential creation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Ask an admin to create you a `PeerTube`_ account.

.. _authorization:

PeerTube authentication: authorization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Run `auth.py --url https://peertube.debian.social/ --account $MY_USERNAME`
   (in the virtualenv, below).
   It will prompt you for your password, and create ``client_id.json``
   and ``token.json``.

Virtualenv creation
~~~~~~~~~~~~~~~~~~~

This uploader requires a few Python libraries (see
``requirements.txt``).
You can either install them from Debian, or create a virtualenv to
contain them.

#. Create a new Python virtualenv::

     python3 -m venv ve
     ve/bin/python -m ensurepip -U
     ve/bin/python -m pip install wheel

#. Install the required packages in the virtualenv::

     ve/bin/python -m pip install -r requirements.txt

#. Activate the virtualenv before using these tools, every time::

     . ve/bin/activate

Import videos from a conference
-------------------------------

#. Upload the videos to the meetings-archive.

#. Describe them in archive-meta.

#. Run ``merge-meta``::

     tools/merge-meta.py ../archive-meta/metadata/

#. A new metadata file should be created with your new playlist.
   Check the contents, edit archive-meta if necessary, and repeat until
   satisfied with the data.

#. Check and update the ``defaults``, applied to every upload.

#. Run the upload script, recording uploaded video IDs to a log.

   The first time you do this, you'll have to :ref:`authorize
   it<authorization>`.

   ::

     tools/upload_peertube.py \
         metadata/2019/example.yml upload.log

#. Merge the video IDs from it into the metadata file::

     tools/merge-uploads.py metadata/2019/example.yml upload.log
